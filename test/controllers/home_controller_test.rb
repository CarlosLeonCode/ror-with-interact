require 'test_helper'

class HomeControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get home_index_url
    assert_response :success
  end

  test "should get exampleOne" do
    get home_exampleOne_url
    assert_response :success
  end

  test "should get exampleTwo" do
    get home_exampleTwo_url
    assert_response :success
  end

end
