document.addEventListener('turbolinks:load',() => { 

    interact('.wrapper-zone').dropzone({
        accept: '.task',
    
        ondropactivate: event => {
            event.target.classList.add('zone-available')
        },


        ondragenter: event => {

            // console.log(event.relatedTarget);
        },

       
        ondrop: event => {
        //    console.log(event.relatedTarget);
        },

        ondropdeactivate: event => {
            event.target.classList.remove('zone-available')
        }
    })
    



    // Tasks 
    interact('.task').draggable({

        listeners: {
            start: event => {
                // console.log(event);
            },

            move: event => {
                // console.log(event);

            },
  
            end: event => {
                if (event.relatedTarget){
                    console.log('ok');
                }else{
                    console.log('error');
                    event.target.style.transform = `translate(${event.dx}px,${event.dy}px)`
                    // console.log(event);
                }
            },

            
            
        },

        

        inertia: true,
        
        // modifiers: [
        //   interact.modifiers.restrictRect({
        //     restriction: '.wrapper-zone',
        //     endOnly: true
        //   })
        // ],
        autoScroll: false,
        // dragMoveListener from the dragging demo above
        onmove: dragMoveListener
    })
})