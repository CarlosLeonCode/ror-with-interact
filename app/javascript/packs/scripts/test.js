$(() => {


    interact('.zone').dropzone({
        accept: '.my-circle',
        overload: 0.5,

        ondrop: function (event) {
            console.log(event);
            alert(event.relatedTarget.id
                + ' was dropped into '
                + event.target.id)
        },

        checker: function(
            dragEvent,         // related dragmove or dragend
            event,             // Touch, Pointer or Mouse Event
            dropped,           // bool default checker result
            dropzone,          // dropzone Interactable
            dropElement,       // dropzone element
            draggable,         // draggable Interactable
            draggableElement
        ){
            // console.log(draggableElement);
            // console.log(dropElement);
            // console.log(dropped);       
            // console.log(dragEvent.type);
        },

        ondropactivate: event => {
            event.target.classList.add('drop-activated')
        },

        ondragenter: event => {
            var draggableElement = event.relatedTarget
       
            // feedback the possibility of a drop
            draggableElement.textContent = 'Dragged in'
        },

        ondrop: function (event) {
            event.relatedTarget.textContent = 'Dropped'
          },



     

        ondropdeactivate: event => {
            event.target.classList.remove('drop-activated')
        }


    })
    // .on('dropactivate', event =>  {
    //     // console.log(event.relatedTarget);       // The element that's being dragged   


    //     
    // })
    
    // .on('ondragenter', event => {
    //     console.log('IT IS ENTER');
    // })

    // .on('dropdeactivate', event => {
    //     event.target.classList.remove('drop-activated')
    // })



    var position = {x: 0, y: 0}
    interact(".my-circle").draggable({
        listeners: {
            start (event){
                // console.log(event.type)         
            },

            move (event){
                position.x += event.dx
                position.y += event.dy
                // transform 
                event.target.style.transform = `translate(${position.x}px,${position.y}px)`
 
            },

            leave (event){
                console.log(event);
            },

            enter(event){
                console.log(event);
            }
            
        }
    })

    interact(".my-circle-2").draggable({


        listeners: {
            start (event){
                console.log(event.type)         
            },

            move(event){
                position.x += event.dx
                position.y += event.dy
                // transform 
                event.target.style.transform = `translate(${position.x}px,${position.y}px)`
 
            }
        }
    })





})