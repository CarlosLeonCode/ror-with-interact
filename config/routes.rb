Rails.application.routes.draw do
  root 'home#index'
  get 'ex_one', to: 'home#exampleOne'
  get 'ex_two', to: 'home#exampleTwo'
end
